# latex-checker


## Description:

This script is built to check some aspects of a latex document. The rules are based on the structure of a proper latex document and on style and syntax rules from various sources (see section sources). The detection mechanism is based on a regex-action pair.  regex is defined to detect a potential mistake. Then a function can be called to further verify if the snippet of text is problematic. Teh function can be used to detect things that regew alone are not able to. For example, checking that a figure is referenced in the document would be difficult in a unique regex.

Example: A rule define that section titles should be in title case (first letter of important words capted). A regex is defined to catch section title (something like `*section{(.+)}`. A regex cannot check the case so a function is called to check the match. If the title is not in titlecase, it is reported to the user.

## Arguments:

positional arguments:
  ifile          Path of the input file.

options:
  -h, --help     show this help message and exit
  -e             Create enriched tex file report.
  -m             Display only a minimal version of the results.
  -c             Enable checking of commented lines.
  --ofile OFILE  Path of the output file.

* -ifile: chemin du fichier à vérifier.

# Sources

* Some rules or features come from the [Textidote](https://github.com/sylvainhalle/textidote) project.
* Some rules are idiosyncrasies from my supervisor S. Fischmeister.
* Some rules come from "Bugs in Writing" by Lyn Dupré.
* Some rules are just from me.
