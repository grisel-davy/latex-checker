# -*- coding: UTF-8 -*-

### Latex Checker
### Author: Arthur 'Grizzly' Grisel-Davy



""" The rules defines the suspicious patterns to search in 
the text. Every rule have a name, pattern, reason why this is suspicious
 and an action to investigate further the matches.
 """

from actions import *

#List of words that I often write wrong
dictionary = ['informations','equipments','wares','requier','technologie','ressource']

rules=[
	{'name':'Titlecase',
	'pattern':r'\\(?:sub)*section\{(.+)\}',
	'reason':'The titles of sections should be in titlecase.',
	'action':check_titlecase},

	{'name':'Caption',
	'pattern':r'\\caption\{(.+)\}',
	'reason':'The caption should be normal sentences (maj and period).',
	'action':check_sentence},

	{'name':'a/an',
	'pattern':r'\s((?:a|an)\s[a-zA-Z]+)\s',
	'reason':'A turn into AN when followed with a vowel.',
	'action':next_start_vowel},

	{'name':'Text between titles',
	'pattern':r'(\\(?:sub)*section\{.+\})(?:\W*\\(?:sub)*section\{.+\})+',
	'reason':'There should be some text between two (sub)sections titles.',
	'action':wrong},

	{'name':'Specific label',
    'pattern':r'(\\label{\w+})',
    'reason':'Labels should have an indicator of its element (tab:, fig:, subsec:...).',
    'action':wrong},

	{'name':'Reference figures',
	'pattern':r'\\label{((?:fig|tab)\S+)}',
	'reason':'All Figures should be referenced in the text.',
	'action':find_ref},

	{'name':'Non-breaking space before references',
	'pattern':r'(\s\\ref\{\S+\})',
	'reason':'A references should have a ~ character (i.e. Figure~\\ref{...} to avoid confusion.',
	'action':wrong},

	{'name':'Misplaced Label',
	'pattern':r'\\begin{\S+}(?:\n.*)+(\\label{\S+}(?:\n.*)+\\caption{\S+})(?:\n.*)+\\end{\S+}',
	'reason':'The label should be placed after the caption to avoid issues.',
	'action':wrong},

    {"name":"Verbization",
     "pattern": r'(ized|izing|ize)\W',
     "reason":"Verbizing nouns is a bad habbit that leads to jargonized speech (BiW segment 22).",
     "action":wrong},

    #{"name":"Start With Number",
    # "pattern":r'^\s*\d+|[.|:]?\s*(\d+)',
    # "reason":"Numbers after point or colon should be speled out.",
    # "action":wrong},

    {"name":"Small Numbers",
     "pattern":r'\W[0-9]\W',
     "reason":"Single digit numbers should be spelled out.",
     "action":wrong},

    {"name":"Capitalized Document Elements",
     "pattern":r'\W?([sub]*(?:section|chapter|figure|table)[ |~]\\ref{[\w:-]*})',
     "reason":"Document elements that are referenced are capitalized",
     "action":wrong},

    {"name":"Dictionary",
    "pattern":r"({})".format('|'.join(dictionary)),
    "reason":"How to say this... You kinda suck at writing in general. No offense.",
    "action":wrong},
]
