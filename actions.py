# -*- coding: UTF-8 -*-

### Latex Checker
### Author: Arthur 'Grizzly' Grisel-Davy


"""Actions are applied on matches to add a layer of verification and decide
if these matches are indeed suspicious. The Action variable will always be 
called so every rule should specifie an action. For rules that does not need an action,
the cannonical action "wrong()" is provided to immediatly identify the match as 
suspicious. By convention, an action return False if the match is indeed wrong."""

def wrong(plop,text):
	"""Cannonical action that immediatly return False (= match is wrong)."""
	return(False)

def check_titlecase(string,text):
	"""Check if the submitted string is titlecase (i.e. Caps at the begining of importants words)."""
	not_maj = ['a', 'an', 'the', 'at', 'by', 'for', 'in', 'of', 'on', 'to', 'up', 'and', 'as', 'but', 'or', 'nor']
	words = string.split(' ')
	res=True
	for word in words:
		if word not in not_maj:
			res = res and word[0].isupper()
	res = res and not string[-1]=='.'
	return(res)

def check_sentence(string,text):
	"""check that the submitted sentence is a correct sentence (Caps and period)."""
	res = string[0].isupper() and string[-1]=="."
	return res

def next_start_vowel(string,text):
	"""Check if the next word begin with a vowel."""
	vowels = ['a','e','i','o','u','y','A','E','I','O','U','Y']
	first = string.split(" ")[0]
	initial = string.split(" ")[1][0]
	return (first == "an" and initial in vowels) or (first == "a" and initial not in vowels)


def find_ref(label,text):
	"""Search for the reference to the submited label in the text."""
	return(f'\\ref{{{label}}}' in text)
