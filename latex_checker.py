#coding: utf-8

# Latex Checker
# Author: Arthur 'Grizzly' Grisel-Davy


import re
import sys
import argparse
import copy
from os.path import dirname, join, exists

from rich.table import Table
from rich.panel import Panel
from rich.console import Console
console = Console()

from rules import *
from actions import *


parser = argparse.ArgumentParser(description='Process inputs')

parser.add_argument('ifile',help='Path of the input file.')
parser.add_argument('-e',action='store_true', help='Create enriched .tex file report with highlighted errors (may not be compilable).')
parser.add_argument('-m',action='store_true', help='Display only a minimal version of the results.')
parser.add_argument('-c',action='store_true', help='Enable checking of commented lines.')
parser.add_argument('-f',action='store_true', help='Follow files inclusion (\input{...}).')
parser.add_argument('--ofile',help='Path of the output file (for enriched output).')
args = parser.parse_args()


if args.ofile == None:
    args.ofile = args.ifile.replace('.tex','_enriched.tex')



file = open(args.ifile,'r')
text = file.read() # get whole text for multiline patterns and search of lines.
file.close()

if args.f:
    # Search for input file and add them
    filenames = re.findall(r'input\{(\w*)\}', text)

    
    for filename in filenames:
        if filename != '':
            path = join(dirname(args.ifile),f"{filename}.tex")
            if exists(path):
                with open(path,'r') as f:
                    text += f.read()

                console.print(f"[bold][green]+[/green] Included file {filename}.tex[/bold]")

if not args.c:
    text = re.sub(r'\n%.+\n',"",text)


def find_line(string):
    """Function to search for the full line from an extract in the text."""
    line = re.findall(r'(^.+{}.+$)'.format(re.escape(string)),text,re.MULTILINE)
    if line != []:
        return line[0]
    else:
        return False

def print_rule(rule):
    """Print a rule nicely."""
    #console.rule(f"[bold] {rule['name']}", style="orange")
    console.print()
    console.print(Panel(f"| [bold]Pattern:[/bold] {rule['pattern']}\n| [bold]Reason:[/bold] {rule['reason']}\n[bold]Results:[/bold]", \
            title=f"Rule: {rule['name']}"))

def print_stat(stats):
    """Print stats at end of check."""
    
    table = Table(title="Results")
    table.add_column("Rule", justify="left")
    table.add_column("Count", justify="center")


    for name in stats.keys():
        count = stats[name]
        if count > 3:
            style = "red"
        elif count <= 3 and count > 0:
            style = "orange1"
        else:
            style = "green"
        table.add_row(name,str(stats[name]), style=style)

    console.print(table)


# Start of the verification loop

stat = {}
for rule in rules:
    name = rule['name']
    pattern = rule['pattern']
    reason = rule['reason']
    action = rule['action']
    if not args.m:
        print_rule(rule)
    counter = 0
    # get all patterns matching in the text
    res_list = re.findall(pattern,text)
    for res in res_list:
        if not action(res,text):
            # If the result of the action is False the pattern in indeed suspicious.
            counter+=1
            # Get the context line from the document.
            ligne = find_line(res)
            if ligne:
                # If we found the line, highlight the patttern.
                ligne = ligne.replace(res,f"[red]{res}[/red]")
            if not args.m:
                # If not instructed to keep it simple, print the suspicious line.
                console.print(ligne)
            if args.e:
                text = text.replace(res,'{{\\color{{red}} {} ({})}}'.format(res,name))
    if not args.m:
        if counter:
            print(f"Found {counter} suspicious elements.")
        else:
            print(f"All good, good job!")

    stat[name]=counter

print_stat(stat)
if args.e:
    file = open(args.ofile,'w')
    file.write(text)
    file.close()

print('Done')
